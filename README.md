# Cookiecutter Data Project

*Project template for data science intensive projects*

## Features

* Directory structure inspired by [DrivenData template](https://github.com/drivendata/cookiecutter-data-science)
* `constants.py` with constants to relative paths
* Logging module (called in `main.py`)
* Creates a Python package that can be installed with `pip`
* `Doxyfile` for generating documentation
* Start developing instantly by running `make install`
* Automatic virtualenv creation
* Git pre-commit hooks: `black`, `flake8` and `isort`

### Makefile

* `clean` removes compiled Python files
* `install` initializes a git repo, creates a virtualenv and installs appropriate packages for developing
* `run` executes the main program with the correct Python interpreter

## Installation and usage

1. Install [Cookiecutter](http://cookiecutter.readthedocs.org/en/latest/installation.html) Python package
2. Run `cookiecutter https://gitlab.com/hhakk/cookiecutter-dataproject`
