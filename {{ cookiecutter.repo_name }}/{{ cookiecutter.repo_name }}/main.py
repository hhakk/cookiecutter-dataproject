#!/usr/bin/env python3

"""
{{ cookiecutter.repo_name }}

Main module for {{ cookiecutter.repo_name }} package

author: {{ cookiecutter.author_name }}

email: {{ cookiecutter.author_email }}
"""

import argparse
import logging

from {{cookiecutter.repo_name}}.utils.log import init_logger


def main() -> None:
    """Main function in the program
    :returns: None
    """
    init_logger()
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m",
        "--mode",
        choices=["preprocess", "train", "test", "visualize", "all"],
        default="all",
        help="preprocess = preprocess dataset, train = train with data, test = test with data,  visualize = visualize dataset, all = do all operations",
    )
    args = parser.parse_args()
    logging.info(f"Project created successfully! Edit main.py to remove this message.")
    logging.info(f"Starting mode '{args.mode}'...")


if __name__ == "__main__":
    main()
