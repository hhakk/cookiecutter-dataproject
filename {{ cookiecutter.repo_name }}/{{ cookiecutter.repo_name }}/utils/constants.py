#!/usr/bin/env python3

"""Project level constants are defined here
"""

from pathlib import Path

# GLOBAL PARAMETERS

# DATA PATHS
DATA = Path(__file__).parents[2].joinpath("data")
DATA_RAW = Path(DATA).joinpath("raw")
DATA_RAW = Path(DATA).joinpath("interim")
DATA_PROCESSED = Path(DATA).joinpath("processed")

# OTHER PATHS
REPORTS = Path(__file__).parents[2].joinpath("reports")
REPORTS_FIGURES = Path(REPORTS).joinpath("figures")
LOGDIR = Path(__file__).parents[2].joinpath("logs")
DOCUMENTATION = Path(__file__).parents[2].joinpath("docs")
REFERENCES = Path(__file__).parents[2].joinpath("references")
MODELS = Path(__file__).parents[2].joinpath("models")
