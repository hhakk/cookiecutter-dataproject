#!/usr/bin/env python3

"""
Initializes a logger for the project
"""

import logging
from datetime import datetime
from pathlib import Path

from {{cookiecutter.repo_name}}.utils import constants


def init_logger() -> None:
    """Init a logger for the project
    :returns: None
    """
    RUNTIME = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

    LOGFILE = Path(constants.LOGDIR).joinpath(RUNTIME + "_log.log")
    logging.basicConfig(
        format="%(asctime)s - %(message)s",
        level=logging.INFO,
        handlers=[logging.FileHandler(LOGFILE), logging.StreamHandler()],
    )
    logging.info(f"Logger initialized at {RUNTIME}")
