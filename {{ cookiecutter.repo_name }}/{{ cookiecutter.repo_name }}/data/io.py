#!/usr/bin/env python3

"""Data input/output related functions"""

from {{cookiecutter.repo_name}}.utils import constants


def load_from_raw_data() -> None:
    """Loads raw data files
    :returns: None
    """
    raw_data_dir = constants.DATA_RAW  # noqa: F841


def load_from_interim() -> None:
    """Loads interim data files
    :returns: None
    """
    interim_data_dir = constants.DATA_INTERIM  # noqa: F841


def load_from_processed_data() -> None:
    """Loads processed data files
    :returns: None
    """
    raw_data_dir = constants.DATA_PROCESSED  # noqa: F841


def save_to_raw_data() -> None:
    """Saves to raw data dir
    :returns: None
    """
    raw_data_dir = constants.DATA_RAW  # noqa: F841


def save_to_interim() -> None:
    """Saves to intermim data dir
    :returns: None
    """
    interim_data_dir = constants.DATA_INTERIM  # noqa: F841


def save_to_processed_data() -> None:
    """Saves to processed data dir
    :returns: None
    """
    raw_data_dir = constants.DATA_PROCESSED  # noqa: F841
