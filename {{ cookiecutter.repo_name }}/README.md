{{cookiecutter.project_name}}
==============================

{{cookiecutter.description}}

# Repo structure

```
.
├── docs <--- Generated Documentation
├── Doxyfile <--- Doxyfile for generating documentation with Doxygen
├── logs <--- Project logs
├── Makefile <--- Makefile for installing and running
├── models <--- Saved models
├── README.md <--- General info, this document
├── references <--- Relevant articles etc.
├── reports <--- Generated reports
│   └── figures <--- Generated figures
├── requirements.txt
├── setup.py <--- To make a Python package
├── {{cookiecutter.repo_name}} <--- The Python package
   ├── data <--- Data I/O and preprocessing
   │   ├── __init__.py
   │   └── io.py
   ├── features <--- Feature extraction
   │   ├── build_features.py
   │   └── __init__.py
   ├── __init__.py
   ├── main.py <--- Main callable program
   ├── models <--- ML models
   │   ├── __init__.py
   │   ├── predict_model.py
   │   └── train_model.py
   ├── utils <--- Utility functions
   │   ├── constants.py
   │   ├── __init__.py
   │   └── log.py
   └── visualization <--- Visualization functions
       ├── __init__.py
       └── visualize.py

```
